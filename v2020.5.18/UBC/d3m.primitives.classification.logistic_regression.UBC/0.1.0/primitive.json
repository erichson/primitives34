{
    "id": "8a992784-3b41-4915-bb47-3ff00e51e60f",
    "version": "0.1.0",
    "name": "Bayesian Logistic Regression",
    "description": "-------------\nInputs:  DataFrame of features of shape: NxM, where N = samples and M = features.\nOutputs: DataFrame containing the target column of shape Nx1 or denormalized dataset.\n-------------\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.classification.logistic_regression.UBC",
    "primitive_family": "CLASSIFICATION",
    "algorithm_types": [
        "LOGISTIC_REGRESSION"
    ],
    "source": {
        "name": "UBC",
        "contact": "mailto:tonyjos@ubc.cs.ca",
        "uris": [
            "https://github.com/plai-group/ubc_primitives.git"
        ]
    },
    "keywords": [
        "bayesian",
        "binary classification"
    ],
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/plai-group/ubc_primitives.git@ba60f83008876788758157b42ca9de5870433c78#egg=ubc_primitives"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives_ubc.logisticRegression.logistic_regression.LogisticRegressionPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "primitives_ubc.logisticRegression.logistic_regression.Params",
            "Hyperparams": "primitives_ubc.logisticRegression.logistic_regression.Hyperparams"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "base.ProbabilisticCompositionalityMixin",
            "base.SamplingCompositionalityMixin",
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "burnin": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1000,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "The number of samples to take before storing them"
            },
            "mu": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0.0,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Mean of Gaussian prior on weights"
            },
            "sd": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1.0,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Standard deviation of Gaussian prior on weights"
            },
            "num_iterations": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1000,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of iterations to sample the model."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives_ubc.logisticRegression.logistic_regression.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "num_samples": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 1
            },
            "params": {
                "type": "primitives_ubc.logisticRegression.logistic_regression.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult",
                "description": "Sample a Bayesian Logistic Regression model using NUTS to find\nsome reasonable weights.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "primitives_ubc.logisticRegression.logistic_regression.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "log_likelihood": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[Outputs]",
                "description": "Returns log probability of outputs given inputs and params under this primitive:\n\nsum_i(log(p(output_i | input_i, params)))\n\nBy default it calls ``log_likelihoods`` and tries to automatically compute a sum, but subclasses can\nimplement a more efficient or even correct version.\n\nParameters\n----------\noutputs:\n    The outputs. The number of samples should match ``inputs``.\ninputs:\n    The inputs. The number of samples should match ``outputs``.\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nsum_i(log(p(output_i | input_i, params))) wrapped inside ``CallResult``.\nThe number of returned samples is always 1.\nThe number of columns should match the number of target columns in ``outputs``."
            },
            "log_likelihoods": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[typing.Sequence[float]]",
                "description": "Provides a likelihood of the data given the weights\n\nParameters\n----------\noutputs:\n    The outputs. The number of samples should match ``inputs``.\ninputs:\n    The inputs. The number of samples should match ``outputs``.\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nlog(p(output_i | input_i, params))) wrapped inside ``CallResult``.\nThe number of columns should match the number of target columns in ``outputs``."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Returns the MAP estimate of p(y | x = inputs; w)\ninputs: (num_inputs,  D) numpy array\noutputs : numpy array of dimension (num_inputs)\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "sample": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "num_samples",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[typing.Sequence[d3m.container.pandas.DataFrame]]",
                "description": "Sample output for each input from ``inputs`` ``num_samples`` times.\n\nSemantics of ``timeout`` and ``iterations`` is the same as in ``produce``.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\nnum_samples:\n    The number of samples to return in a set of samples.\ntimeout:\n    A maximum time this primitive should take to sample outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe multiple sets of samples of shape [num_samples, num_inputs, ...] wrapped inside\n``CallResult``. While the output value type is specified as ``Sequence[Outputs]``, the\noutput value can be in fact any container type with dimensions/shape equal to combined\n``Sequence[Outputs]`` dimensions/shape. Subclasses should specify which exactly type\nthe output is."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets current training data of this primitive.\n\nThis marks training data as changed even if new training data is the same as\nprevious training data.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "weights": "d3m.container.numpy.ndarray"
        }
    },
    "structural_type": "primitives_ubc.logisticRegression.logistic_regression.LogisticRegressionPrimitive",
    "digest": "5d5064f84d8464ff47ef6a8a91c769eaf699a9dc4a15c0ebbdaf137d510d47c4"
}
